/**
 * @author Alper GULEC - Test Automation Engineer
 *
 * The email address, password and browser information read from the config.properties file were used for the written test cases.
 * In addition, allure test integration was performed.
 * The test outputs the report after the run.
 * "allure serve allure-results" is written in the terminal and the report can be viewed in html format.
 * Using slf4j, logging was performed at error and info level.
 * "chrome driver.exe " is read in the project.
 *
 * */

import base.TestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;

public class TestCases extends BaseTest {

    private String selectedProductName;
    private static final String searchText = "samsung";
    private static final String pageNumberToNavigate = "2";
    private static final String searchPageTitle = "Samsung - n11.com - 2/50";
    private static  Logger logger = LoggerFactory.getLogger(TestCases.class);


    @Test(priority = 1, description = " User go to n11 homepage and validate n11 homepage's title", groups = {"Home"})
    public void homePageTest_VerifyHomePage() {

        HomePage homePage = new HomePage(driver, wait);
        homePage.goToHomePage();
        homePage.checkOfOnPage();

        if (homePage.checkOfOnPage() != true) {

            logger.error(" [ ERROR ] [ YOU ARE NOT ON THE HOME PAGE ] [ PLEASE CHECK THE PAGE TITLE ] ");
        }
        Assert.assertTrue(homePage.checkOfOnPage());
    }

    @Test(priority = 1, description = " The user clicks the login button on the homepage and login with email and password", groups = {"Login"})
    public void loginTest_UserNamePassword() {

        HomePage homePage = new HomePage(driver, wait);
        homePage.goToLoginPage();
        LoginPage loginPage = new LoginPage(driver, wait);

        String emailAdres = TestBase.prop().getProperty("emailAdress");
        String password = TestBase.prop().getProperty("password");

        logger.info(" [ INFO ] [ EMAIL INFORMATION TO BE USED FOR LOGIN ] : {} [ PASSWORD INFORMATION TO BE USED FOR LOGIN ] : {}" ,emailAdres,password);

        loginPage.loginToN11(emailAdres, password);
    }

    @Test(priority = 2, description = " The user searches for samsung in the search field.", groups = {"Search"})
    public void searchProduct_VerifySearchFound() {
        SearchPage searchPage = new SearchPage(driver, wait);
        searchPage.searchProduct(searchText);

        if (searchPage.checkAnyResultFound() != false) {

            logger.error(" [ ERROR ] [ THE NUMBER OF {} PRODUCTS SEARCHED ON THE SEARCH PAGE MUST BE GREATER THAN 0 ]", searchText);
        }
        Assert.assertFalse(searchPage.checkAnyResultFound());
    }

    @Test(priority = 3, description = "After the user searches, it opens the second page from the listed products page.", groups = {"Search"})
    public void navigateToPage_VerifyPageTitle() {
        SearchPage searchPage = new SearchPage(driver, wait);
        searchPage.navigateToPage(searchText, pageNumberToNavigate);

        if (!searchPage.getPageTitle().equals(searchPageTitle)) {

            logger.error(" [ ERROR ] [ THE TITLE ON THE SEARCH PAGE MUST BE ] :"+ searchPageTitle);
        }
        Assert.assertEquals(searchPage.getPageTitle(), searchPageTitle);
    }

    @Test(priority = 4, description = "The user selects the third product from the search results and adds it to favorites.", groups = {"Favorites"})
    public void addToFavorites_verifySelectedProduct() {

        SearchPage searchPage = new SearchPage(driver, wait);
        searchPage.selectProduct();

        ProductListPage productPage = new ProductListPage(driver, wait);
        productPage.checkRequiredFields();

        String[] selectedProduct = productPage.addToFavorites();
        selectedProductName = selectedProduct[0];
        String selectedProductIdToCheck = selectedProduct[1];

        FavoriteListPage favoritesPage = new FavoriteListPage(driver, wait);

        if (!favoritesPage.getProductName(selectedProductIdToCheck).equals(selectedProduct)) {

            logger.error(" [ ERROR ] [ THE PRODUCT ADDED TO THE FAVORITES AND THE PRODUCT NOT IN THE FAVORITE LIST ]" +
                    " [ THE PRODUCT NAME OF ADDED TO FAVORITE ] : {} [ FAVORITE LIST PRODUCT ] : {}", favoritesPage.getProductName(selectedProductIdToCheck), selectedProduct);
        }
        logger.info(" [ INFO ] [ IT IS CHECKED TO BE THE SAME WITH THE PRODUCT ADDED TO THE FAVORITE LIST WITH THE SELECTED PRODUCT ]");

        Assert.assertEquals(favoritesPage.getProductName(selectedProductIdToCheck), selectedProductName);
    }

    @Test(priority = 4, description = "The user deletes the product he added to his favorites.", groups = {"Favorites"}, dependsOnMethods = "addToFavorites_verifySelectedProduct")
    public void deleteFromFavorites_verifyDeletion() {
        FavoriteListPage favoritesPage = new FavoriteListPage(driver, wait);
        favoritesPage.deleteFromMyFavoriteList();

        if (favoritesPage.FavoriteDeletedCheck(selectedProductName) != false) {

            logger.error(" [ ERROR ] [ FAVORITE LIST PRODUCT HAS NOT DELETED ] " +
                    "[ SELECTED PRODUCT NAME FOR DELETE FAVORITE LIST] : {}", selectedProductName);
        }
        logger.info(" [ INFO ] [ PRODUCT SUCCESSFULLY DELETED FROM FAVORITE LIST ] [ DELETED PRODUCT NAME ] : {}", selectedProductName);

        Assert.assertFalse(favoritesPage.FavoriteDeletedCheck(selectedProductName));
    }
}
