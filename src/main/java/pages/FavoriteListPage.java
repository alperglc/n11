package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class FavoriteListPage extends TestBase {

    private static final String deleteButtonClass = "deleteProFromFavorites";

    public FavoriteListPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
    public String getProductName(String ProductId) {

        driver.get(prop().getProperty("url") + "/hesabim/favorilerim");
        String productSelector = "#p-" + ProductId;
        return driver.findElement(By.cssSelector(productSelector + " > div > a > h3")).getText();
    }

    public void deleteFromMyFavoriteList() {
        driver.findElement(By.className(deleteButtonClass)).click();
    }

    public boolean FavoriteDeletedCheck(String ProductName) {

        List<String> favoriteList = new ArrayList<String>();
        List<WebElement> favoriteProducts = driver.findElements(By.xpath("//*[@class='productTitle']/p/a"));
        for (WebElement productTitle : favoriteProducts) {
            String nameOfProduct = productTitle.getText();
            favoriteList.add(nameOfProduct);
        }
        return favoriteList.contains(ProductName);
    }

}
