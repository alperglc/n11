package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage extends TestBase {

    private static final String searchFieldId = "searchData";
    private static final String searchButtonCss = "a.searchBtn";

    public SearchPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void searchProduct(String searchText) {

        driver.findElement(By.id(searchFieldId)).click();
        driver.findElement(By.id(searchFieldId)).clear();
        driver.findElement(By.id(searchFieldId)).sendKeys(searchText);
        driver.findElement(By.cssSelector(searchButtonCss)).click();
    }

    public boolean checkAnyResultFound() {
        return (driver.findElements(By.className("notFoundContainer")).size() != 0);
    }

    public void navigateToPage(String textForSearched, String pageNumber) {
        String pageURL = prop().getProperty("url") + "/arama?q=" + textForSearched + "&pg=" + pageNumber;
        driver.navigate().to(pageURL);
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void selectProduct() {

        String selectedProductLink = driver.findElement(By.cssSelector("#view > ul > li:nth-child(3) > div > div > a[href]")).getAttribute("href");
        driver.get(selectedProductLink);
    }
}
